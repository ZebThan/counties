class PublicApi::LocationsController < ApplicationController

  def index
    country = Country.find_by!(country_code: params[:country_code])
    render json: country.locations.to_json
  end

end