class PublicApi::TargetGroupsController < ApplicationController

  def index
    country = Country.find_by!(country_code: params[:country_code])
    render json: country.target_groups.to_json
  end

end