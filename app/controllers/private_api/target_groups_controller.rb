class PrivateApi::TargetGroupsController < PrivateApi::ApiController

  def index
    country = Country.find_by!(country_code: params[:country_code])
    response = country.target_groups.where(panel_provider_id: current_user.panel_provider.id)

    render json: response.to_json
  end

end