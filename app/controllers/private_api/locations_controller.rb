class PrivateApi::LocationsController < PrivateApi::ApiController

  def index
    country = Country.find_by!(country_code: params[:country_code])
    response = country.locations.joins(:location_groups)
               .where('location_groups.panel_provider_id = ?', current_user.panel_provider.id)

    render json: response.to_json
  end   

end