class PrivateApi::PricesController < PrivateApi::ApiController
  
  def evaluate
  	render json: {message: "Invalid parameters" } and return if params_invalid?
  	panel_provider = Country.find_by!(country_code: params[:country_code]).panel_provider
  	render json: { price: panel_provider.get_price }.to_json
  end

  private

  def params_invalid?
  	required_params_absent? || locations_params_invalid?
  end

  def required_params_absent?
  	(required_keys - params.keys).present?
  end

  def required_keys
  	['country_code', 'target_group_id', 'locations']
  end

  def locations_params_invalid?
    params[:locations] != params[:locations].map{ |element| {'id' => element[:id], 'panel_size' => element[:panel_size] } }
  end



end