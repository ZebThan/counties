class PrivateApi::ApiController < ApplicationController
  
  before_action :sign_out_if_needed
  before_action :auth_or_sign_in

  def sign_out_if_needed
  	sign_out if params[:sign_out]
  end

  def auth_or_sign_in
    if current_user.nil?
      sign_out if current_user.present?
      allow_params_authentication!
      render json: { message: 'Authentication Error' }, status: 403 and return unless warden.authenticate
    end
  end

end