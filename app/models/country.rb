class Country < ActiveRecord::Base
  belongs_to :panel_provider

  has_and_belongs_to_many :target_groups, join_table: :countries_target_groups_joins

  has_many :locations, through: :panel_provider
end