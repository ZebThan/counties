class Location < ActiveRecord::Base
  has_and_belongs_to_many :location_groups, join_table: :locations_groups_joins
end
