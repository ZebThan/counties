class PanelProvider < ActiveRecord::Base
  has_many :countries

  has_many :location_groups
  has_many :locations, through: :location_groups

  PRICE_CALCULATORS = ['V1', 'V2', 'V3']

  def get_price
    return "Can't find calculator version: #{calc_version}" unless PRICE_CALCULATORS.include?(calc_version)
  	"PriceCalculators::#{calc_version}".constantize.new.call
  end
end