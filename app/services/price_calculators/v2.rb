class PriceCalculators::V2
  require 'open-uri'
  
  # Doesn't work for some reason
  # attr_accessor :counter

  def initialize
  	@counter = 0
  end

  def counter
    @counter
  end

  def call
   source = open('http://openlibrary.org/search.json?q=the+lord+of+the+rings'){|f|f.read}
   json = JSON.parse(source)
   search_for_arrays(json)
   counter 
  end

  def search_for_arrays(collection)
    collection.each do |element|
      if element.is_a?(Array) && element.size > 10
        @counter = @counter + 1
      end
      if element.respond_to?(:each)
      	search_for_arrays(element)
      end
    end
  end

end