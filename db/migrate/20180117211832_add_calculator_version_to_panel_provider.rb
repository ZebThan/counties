class AddCalculatorVersionToPanelProvider < ActiveRecord::Migration
  def change
  	add_column :panel_providers, :calc_version, :string
  end
end
