class AddPanelProviderIdToUser < ActiveRecord::Migration
  def change
    add_column :users, :panel_provider_id, :integer
  end
end
