class CreateLocationsGroupsJoin < ActiveRecord::Migration
  def change
    create_table :locations_groups_joins, id: false do |t|
      t.integer :location_id
      t.integer :location_group_id
    end
  end
end
