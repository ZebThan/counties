def create_target_group_children(target_group, target_level, amount = 2, current_level = 1 )
  amount.times do |n|
    new_child = TargetGroup.create(name: "#{target_group.name}/#{n}")
    target_group.children << new_child
    p current_level
    create_target_group_children(new_child, target_level, amount, current_level + 1) unless current_level >= target_level
  end 
end

panel_providers = Array.new(3) do |n|
  PanelProvider.create(code: "code-#{n}", calc_version: "V#{n+1}")
end

countries = Array.new(3) do |n|
	Country.create(country_code: "code-#{n}", panel_provider: panel_providers[n])
end

location_groups = Array.new(4) do |n|
  LocationGroup.create(name: "Location Group #{n}", country: countries[n%3], panel_provider: panel_providers[n%3])
end

locations = Array.new(20) do |n|
  location = Location.new(name: "Location #{n}", secret_code: "secret code #{n}")
  rand(1..2).times {|nn| location.location_groups << location_groups[(n+nn)%4] }
  location.save
end

root_target_groups = Array.new(4) do |n|
  TargetGroup.create(name: "Target Group #{n}", panel_provider: panel_providers[n%3])
end

root_target_groups.each_with_index do |rtg, n|
  rand(1..2).times { |nn| rtg.countries << countries[(n+nn)%3] }
  create_target_group_children(rtg, 4)
end

panel_providers.each do |pp|
  User.create(email: "#{pp.code}@email.com", password: 'password123', panel_provider: pp)
end