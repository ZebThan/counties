Rails.application.routes.draw do
  devise_for :users

  namespace :public_api do
    get 'locations/:country_code', to: 'locations#index'
    get 'target_groups/:country_code', to: 'target_groups#index'
  end

  namespace :private_api do
    get 'locations/:country_code', to: 'locations#index'
    get 'target_groups/:country_code', to: 'target_groups#index'
    get 'evaluate_target', to: 'prices#evaluate'
  end
end
